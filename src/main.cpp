#include "kinectCrossRegistration.hpp"

#include <string>


void main(int argc, char* argv[]) {
	
	std::string RGBFileName;
	std::string DFileName;
	std::string outFileName;
	
	if (argc != 4) {
		printf("Usage is\n"
		"kinectCrossRegistration.exe RGBFileName DepthFileName outFileName");
		exit(0);
	}
	else {
		RGBFileName = argv[1];
		DFileName = argv[2];
		outFileName = argv[3];
	}
	
	
	KinectCrossRegistration k(RGBFileName, DFileName);
	
	k.doRegistration(outFileName);
}