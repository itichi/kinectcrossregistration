#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/registration.h>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

class KinectCrossRegistration {
	KinectCrossRegistration(const char * RGBFileName, const char * DFileName)
	{
		int x, y, n = -1;
		RGBData = stbi_load(RGBFileName, &x, &y, &n, 0);
		if (!RGBData) {
			printf("Error: RGB image is void");
			exit(1);
		}
		if (x <= 0 || y <= 0) {
			printf("Error in the RGB image size");
			exit(1);
		}
		RGBData = new libfreenect2::Frame(x,y,n);
		
		x = y = n = -1;
		DData = stbi_load(DFileName, &x, &y, &n, 0);
		if (depth_width <= 0 || depth_height <= 0) {
			printf("Error in the depth image size");
			exit(1);
		}
		
		
		if (!m_Depth) {
			printf("Error: Depth image is void");
			exit(1);
		}
		
		m_RGB
	}
	
	~KinectCrossRegistration() {
		stbi_image_free(RGBData);
		stbi_image_free(DData);
	}
	
	void doRegistration(std::string fileName) {
		libfreenect2::Frame undistorted(m_RGB.x, m_RGB.y, 4);
		libfreenect2::Frame registered(m_RGB.x, m_RGB.y, 4);
		
		registration->apply(m_RGB, m_Depth, undistorded, registered);
	}
	
	
	
private:
	unsigned char * RGBData = nullptr;
	unsigned char * DData = nullptr;
	
	libfreenect2::Frame *m_RGB = nullptr;
	libfreenect2::Frame *m_depth = nullptr;
};